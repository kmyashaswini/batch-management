var express=require('express');
var express_graphql = require('express-graphql').graphqlHTTP;
var {buildSchema} = require('graphql');

// GraphQL Schema

var schema = buildSchema(`

    type Query{
        batch(name:String!) : Batch
        allbatch: [Batch]
    }
    type Mutation{
        createBatch(name:String!, sylid:String!,students:[String!],batchtime:time!,days:[String!],currentstatus:String!):Batch
        editBatch(name:String!, sylid:String!,students:[String!],batchtime:time!,days:[String!],currentstatus:String!):Batch
    }
  input time{
    starttime:String!,
    endtime:String!
  }
    type Batch{
        name:String
        sylid:String
        students:[String]
        batchtime:btime
        days:[String]
        currentstatus:String
    }
    type btime{
        starttime:String
        endtime:String
    }
    type Syllabus{
        sylid:String
        sylname:String
        stages:[String]
    }
    
`);

var Batches=[
    {
        name : "FirstBatch",
        sylid:"J01",
        students :["Manasvi","Shreedhar","Amulya","Anoop"],
        batchtime:{
            starttime:"10:00AM",
            endtime:"12:00PM"
        },
        days:["MON","WED","SUN"],
        currentstatus:"started"
    },
    {
        name : "SecondBatch",
        sylid:"DS01",
        students :["Prashanth","Murli","Sameeksha","Swarna"],
        batchtime:{
            starttime:"5:00PM",
            endtime:"7:00PM"
        },
        days:["TUE","THU","SAT"],
        currentstatus:"started"
    }
]
var syllabus = [
    {
        sylid:"J01",
        sylname:"Java",
        stages:["Basics Understanding of Java" , "Handson On Java"]

    },
    {
        sylid:"DS01",
        sylname:"Data Structure",
        stages:["Basics Understanding of DS" , "Handson On DS"]

    }

]
var getBatch =function(args){
      var name = args.name;
      return Batches.filter(batch => {
        return batch.name == name
      })[0];
}
var getallBatch=function(){
    return Batches;
}
var createbatch =function({name,sylid,students,batchtime,days,currentstatus}){
 Batches.push({
    name:name,
    sylid:sylid,
    students:students,
    batchtime:batchtime,
    days:days,
    currentstatus:currentstatus
 });
 return Batches.filter(newbatch => newbatch.name == name)[0];
}

var editBatch=function({name,sylid,students,batchtime,days,currentstatus}){
    Batches.map(batch => {
        if (batch.name === name) {
            batch.students = students;
            batch.sylid =sylid;
            batch.batchtime = batchtime;
            batch.days = days;
            batch.currentstatus = currentstatus;
            return batch;
        }
        });
        return Batches.filter(batch => batch.name === name) [0];
}
var root={
  batch: getBatch,
  allbatch:getallBatch,
  createBatch : createbatch,
  editBatch:editBatch,
}

//create express and graphql server
var app=express();
app.use('/graphql',express_graphql({
    schema:schema,
    rootValue:root,
    graphiql:true

}));

app.listen(4200,()=>console.log("Running"));